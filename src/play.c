#include "celika/celika.h"
#include "ld35.h"

typedef struct ast_vert_t {
    float x, y;
    float norm_x, norm_y;
} ast_vert_t;

typedef struct asteroid_t {
    list_t* vertices; //list_t of ast_vert_t
    float cx, cy;
    float vel_x, vel_y;
    float angle;
    float rotate_speed;
    float size;
} asteroid_t;

typedef struct player_t {
    float x, y;
    float vel_x, vel_y;
    float angle;
    float health;
    float fire_timeout;
    float score;
} player_t;

typedef struct proj_t {
    float x, y;
    float vel_x, vel_y;
    float dir_x, dir_y;
    float angle;
} proj_t;

static list_t* projectiles; //list_t of proj_t
static list_t* asteroids; //list_t of asteroid_t
static player_t player;
static float difficulty;

static float dist(float x0, float y0, float x1, float y1) {
    return sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0));
}

static bool is_point_on_line(float x0, float y0, float x1, float y1, float px, float py) {
    //TODO: This could probably return true when it should not
    //https://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment/328193#328193
    return fabs(dist(x0, y0, px, py)+dist(x1, y1, px, py)-dist(x0, y0, x1, y1)) < 0.0001;
}

static bool line_vs_line(float x0_0, float y0_0, float x1_0, float y1_0,
                         float x0_1, float y0_1, float x1_1, float y1_1) {
    float x1 = x0_0;
    float y1 = y0_0;
    float x2 = x1_0;
    float y2 = y1_0;
    float x3 = x0_1;
    float y3 = y0_1;
    float x4 = x1_1;
    float y4 = y1_1;
    
    //Some magic taken from https://en.wikipedia.org/wiki/Line-line_intersection#Given_two_points_on_each_line
    float ix = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4)) /
               ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
    float iy = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4)) /
               ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
    
    return is_point_on_line(x0_0, y0_0, x1_0, y1_0, ix, iy) &&
           is_point_on_line(x0_1, y0_1, x1_1, y1_1, ix, iy);
}

static bool point_vs_tri(float px, float py, float x0, float y0,
                         float x1, float y1, float x2, float y2) {
    //Magical stuff from http://www.blackpawn.com/texts/pointinpoly/default.html
    float v0x = x2 - x0;
    float v0y = y2 - y0;
    float v1x = x1 - x0;
    float v1y = y1 - y0;
    float v2x = px - x0;
    float v2y = py - y0;
    
    float dot00 = v0x*v0x + v0y*v0y;
    float dot01 = v0x*v1x + v0y*v1y;
    float dot02 = v0x*v2x + v0y*v2y;
    float dot11 = v1x*v1y + v1y*v1y;
    float dot12 = v1x*v2x + v1y*v2y;
    
    float denom = dot00*dot11 - dot01*dot01;
    float u = (dot11*dot02 - dot02*dot12) / denom;
    float v = (dot00*dot12 - dot01*dot02) / denom;
    
    return u>=0 && v>=0 && u+v<1;
}

static bool tri_vs_tri(const float* tri0, const float* tri1) {
    float tri0_left = fmin(tri0[0], fmin(tri0[2], tri0[4]));
    float tri0_right = fmax(tri0[0], fmax(tri0[2], tri0[4]));
    float tri0_bottom = fmin(tri0[1], fmin(tri0[3], tri0[5]));
    float tri0_top = fmax(tri0[1], fmax(tri0[3], tri0[5]));
    float tri1_left = fmin(tri1[0], fmin(tri1[2], tri1[4]));
    float tri1_right = fmax(tri1[0], fmax(tri1[2], tri1[4]));
    float tri1_bottom = fmin(tri1[1], fmin(tri1[3], tri1[5]));
    float tri1_top = fmax(tri1[1], fmax(tri1[3], tri1[5]));
    
    if (!aabb_intersect(aabb_create_lbrt(tri0_left, tri0_bottom, tri0_right, tri0_top),
                        aabb_create_lbrt(tri1_left, tri1_bottom, tri1_right, tri1_top)))
        return false;
    
    float t0_e0_x0 = tri0[0];
    float t0_e0_y0 = tri0[1];
    float t0_e0_x1 = tri0[2];
    float t0_e0_y1 = tri0[3];
    float t0_e1_x0 = tri0[0];
    float t0_e1_y0 = tri0[1];
    float t0_e1_x1 = tri0[4];
    float t0_e1_y1 = tri0[5];
    float t0_e2_x0 = tri0[2];
    float t0_e2_y0 = tri0[3];
    float t0_e2_x1 = tri0[4];
    float t0_e2_y1 = tri0[5];
    
    float t1_e0_x0 = tri1[0];
    float t1_e0_y0 = tri1[1];
    float t1_e0_x1 = tri1[2];
    float t1_e0_y1 = tri1[3];
    float t1_e1_x0 = tri1[0];
    float t1_e1_y0 = tri1[1];
    float t1_e1_x1 = tri1[4];
    float t1_e1_y1 = tri1[5];
    float t1_e2_x0 = tri1[2];
    float t1_e2_y0 = tri1[3];
    float t1_e2_x1 = tri1[4];
    float t1_e2_y1 = tri1[5];
    
    if (line_vs_line(t0_e0_x0, t0_e0_y0, t0_e0_x1, t0_e0_y1,
                     t1_e0_x0, t1_e0_y0, t1_e0_x1, t1_e0_y1))
        return true;
    if (line_vs_line(t0_e0_x0, t0_e0_y0, t0_e0_x1, t0_e0_y1,
                     t1_e1_x0, t1_e1_y0, t1_e1_x1, t1_e1_y1))
        return true;
    if (line_vs_line(t0_e0_x0, t0_e0_y0, t0_e0_x1, t0_e0_y1,
                     t1_e2_x0, t1_e2_y0, t1_e2_x1, t1_e2_y1))
        return true;
    if (line_vs_line(t0_e1_x0, t0_e1_y0, t0_e1_x1, t0_e1_y1,
                     t1_e0_x0, t1_e0_y0, t1_e0_x1, t1_e0_y1))
        return true;
    if (line_vs_line(t0_e1_x0, t0_e1_y0, t0_e1_x1, t0_e1_y1,
                     t1_e1_x0, t1_e1_y0, t1_e1_x1, t1_e1_y1))
        return true;
    if (line_vs_line(t0_e1_x0, t0_e1_y0, t0_e1_x1, t0_e1_y1,
                     t1_e2_x0, t1_e2_y0, t1_e2_x1, t1_e2_y1))
        return true;
    if (line_vs_line(t0_e2_x0, t0_e2_y0, t0_e2_x1, t0_e2_y1,
                     t1_e0_x0, t1_e0_y0, t1_e0_x1, t1_e0_y1))
        return true;
    if (line_vs_line(t0_e2_x0, t0_e2_y0, t0_e2_x1, t0_e2_y1,
                     t1_e1_x0, t1_e1_y0, t1_e1_x1, t1_e1_y1))
        return true;
    if (line_vs_line(t0_e2_x0, t0_e2_y0, t0_e2_x1, t0_e2_y1,
                     t1_e2_x0, t1_e2_y0, t1_e2_x1, t1_e2_y1))
        return true;
    
    if (point_vs_tri(tri0[0], tri0[1], tri1[0], tri1[1],
                     tri1[2], tri1[3], tri1[4], tri1[5]))
        return true;
    if (point_vs_tri(tri0[2], tri0[3], tri1[0], tri1[1],
                     tri1[2], tri1[3], tri1[4], tri1[5]))
        return true;
    if (point_vs_tri(tri0[4], tri0[5], tri1[0], tri1[1],
                     tri1[2], tri1[3], tri1[4], tri1[5]))
        return true;
    if (point_vs_tri(tri1[0], tri1[1], tri0[0], tri0[1],
                     tri0[2], tri0[3], tri0[4], tri0[5]))
        return true;
    if (point_vs_tri(tri1[2], tri1[3], tri0[0], tri0[1],
                     tri0[2], tri0[3], tri0[4], tri0[5]))
        return true;
    if (point_vs_tri(tri1[4], tri1[5], tri0[0], tri0[1],
                     tri0[2], tri0[3], tri0[4], tri0[5]))
        return true;
    
    return false;
}

static bool line_vs_tri(float* tri, float x0, float y0, float x1, float y1) {
    float tri_left = fmin(tri[0], fmin(tri[2], tri[4]));
    float tri_right = fmax(tri[0], fmax(tri[2], tri[4]));
    float tri_bottom = fmin(tri[1], fmin(tri[3], tri[5]));
    float tri_top = fmax(tri[1], fmax(tri[3], tri[5]));
    
    float line_left = fmin(x0, x1);
    float line_right = fmax(x0, x1);
    float line_bottom = fmin(y0, y1);
    float line_top = fmax(y0, y1);
    
    if (!aabb_intersect(aabb_create_lbrt(tri_left, tri_bottom, tri_right, tri_top),
                        aabb_create_lbrt(line_left, line_bottom, line_right, line_top)))
        return false;
    
    float e0_x0 = tri[0];
    float e0_y0 = tri[1];
    float e0_x1 = tri[2];
    float e0_y1 = tri[3];
    float e1_x0 = tri[0];
    float e1_y0 = tri[1];
    float e1_x1 = tri[4];
    float e1_y1 = tri[5];
    float e2_x0 = tri[2];
    float e2_y0 = tri[3];
    float e2_x1 = tri[4];
    float e2_y1 = tri[5];
    
    if (line_vs_line(e0_x0, e0_y0, e0_x1, e0_y1, x0, y0, x1, y1))
        return true;
    if (line_vs_line(e1_x0, e1_y0, e1_x1, e1_y1, x0, y0, x1, y1))
        return true;
    if (line_vs_line(e2_x0, e2_y0, e2_x1, e2_y1, x0, y0, x1, y1))
        return true;
    
    if (point_vs_tri(x0, y0, tri[0], tri[1], tri[2], tri[3], tri[4], tri[5]))
        return true;
    if (point_vs_tri(x1, y1, tri[0], tri[1], tri[2], tri[3], tri[4], tri[5]))
        return true;
    
    return false;
}

static float tri_area(float x0, float y0, float x1, float y1, float x2, float y2) {
    return fabs((x0-x2)*(y1-y0) - (x0-x1)*(y2-y0)) / 2;
}

static void rotate_point(float* x, float* y, float angle) {
    float x_ = *x;
    float y_ = *y;
    float s = sin(angle/180*3.14159);
    float c = cos(angle/180*3.14159);
    *x = x_*c - y_*s;
    *y = x_*s + y_*c;
}

static void rotate_tri(float* tri, float angle, float ox, float oy) {
    float s = sin(angle/180*3.14159);
    float c = cos(angle/180*3.14159);
    for (size_t i = 0; i < 6; i+=2) {
        float x = tri[i] - ox;
        float y = tri[i+1] - oy;
        tri[i] = x*c - y*s + ox;
        tri[i+1] = x*s + y*c + oy;
    }
}

static bool player_vs_asteroid(asteroid_t* a) {
    float player_pos[] = {-PLAYER_WIDTH/2+player.x, player.y,
                          PLAYER_WIDTH/2+player.x, player.y,
                          player.x, player.y+PLAYER_HEIGHT};
    
    rotate_tri(player_pos, player.angle, player.x, player.y);
    
    size_t c = list_len(a->vertices);
    for (size_t i = 0; i < c; i++) {
        ast_vert_t* v0 = list_nth(a->vertices, i);
        ast_vert_t* v1 = list_nth(a->vertices, (i+1)%c);
        float tri[] = {v0->x+a->cx, v0->y+a->cy, v1->x+a->cx, v1->y+a->cy,
                       a->cx, a->cy};
        rotate_tri(tri, a->angle, a->cx, a->cy);
        
        if (tri_vs_tri(tri, player_pos))
            return true;
    }
    
    return false;
}

static bool line_vs_asteroid(asteroid_t* a, float x0, float y0, float x1, float y1) {
    size_t c = list_len(a->vertices);
    for (size_t i = 0; i < c; i++) {
        ast_vert_t* v0 = list_nth(a->vertices, i);
        ast_vert_t* v1 = list_nth(a->vertices, (i+1)%c);
        float tri[] = {v0->x+a->cx, v0->y+a->cy, v1->x+a->cx, v1->y+a->cy,
                       a->cx, a->cy};
        rotate_tri(tri, a->angle, a->cx, a->cy);
        
        if (line_vs_tri(tri, x0, y0, x1, y1))
            return true;
    }
    
    return false;
}

static void init_player() {
    player.x = WINDOW_WIDTH / 2;
    player.y = WINDOW_HEIGHT / 2;
    player.vel_x = player.vel_y = 0;
    player.angle = rand() % 360;
    player.health = 1;
    player.fire_timeout = 0;
    player.score = 0;
}

static asteroid_t* create_asteroid(float size) {
    size_t vert_count = 8;
    
    float radius = rand() % ((int)size-20) + 20;
    float posx = rand() % WINDOW_WIDTH;
    float posy = rand() % WINDOW_HEIGHT;
    
    list_t* verts = list_create(sizeof(ast_vert_t));
    for (size_t i = 0; i < vert_count; i++) {
        float a = i / (float)vert_count * 3.14159 * 2;
        a += (rand() % (360/vert_count/2)) / 180.0 * 3.14159;
        ast_vert_t v;
        v.norm_x = v.x = cos(a);
        v.norm_y = v.y = sin(a);
        v.x = v.x * radius;
        v.y = v.y * radius;
        list_append(verts, &v);
    }
    
    asteroid_t ast;
    ast.vertices = verts;
    ast.cx = posx;
    ast.cy = posy;
    ast.angle = rand() % 360;
    ast.rotate_speed = rand() % 60;
    ast.size = size;
    while (true) {
        ast.vel_x = rand()/(float)RAND_MAX*2.0 - 1.0;
        ast.vel_y = rand()/(float)RAND_MAX*2.0 - 1.0;
        float len = sqrt(ast.vel_x*ast.vel_x + ast.vel_y*ast.vel_y);
        if (len != 0) {
            ast.vel_x /= len;
            ast.vel_y /= len;
            float speed = rand()%15 + 45;
            ast.vel_x *= speed;
            ast.vel_y *= speed;
            break;
        }
    }
    
    return list_append(asteroids, &ast);
}

static void break_asteroid(asteroid_t* a, float dx, float dy) {    
    for (size_t i = 0 ; i < 2; i++) {
        if (list_len(a->vertices) <= 2) break;
        
        asteroid_t ast;
        size_t j = i ? list_len(a->vertices)/2 : 0;
        size_t e = i ? list_len(a->vertices) : list_len(a->vertices)/2;
        if (!(e-j)) continue;
        ast.vertices = list_create(sizeof(ast_vert_t));
        for (; j < e; j++) {
            ast_vert_t v = *(ast_vert_t*)list_nth(a->vertices, j%list_len(a->vertices));
            v.x /= 1.5;
            v.y /= 1.5;
            list_append(ast.vertices, &v);
        }
        
        ast.cx = a->cx;
        ast.cy = a->cy;
        ast.angle = a->angle;
        ast.rotate_speed = rand() % 60;
        ast.size = a->size;
        float speed = rand()%15 + 45;
        ast.vel_x = dy * speed;
        ast.vel_y = -dx * speed;
        if (i) {
            ast.vel_x *= -1;
            ast.vel_y *= -1;
        }
        
        list_append(asteroids, &ast);
    }
    
    list_del(a->vertices);
    list_remove(a);
}

static float asteroid_area(asteroid_t* a) {
    float res = 0;
    for (size_t i = 0; i < list_len(a->vertices); i++) {
        ast_vert_t* a0 = list_nth(a->vertices, i);
        ast_vert_t* a1 = list_nth(a->vertices, (i+1)%list_len(a->vertices));
        res += tri_area(a0->x, a0->y, a1->x, a1->y, a->cx, a->cy);
    }
    return res;
}

static void create_asteroids() {
    for (size_t i = 0; i < 10; i++)
        create_asteroid(50);
}

void play_init() {
    init_player();
    
    projectiles = list_create(sizeof(proj_t));
    asteroids = list_create(sizeof(asteroid_t));
    create_asteroids();
    
    difficulty = 10;
}

void play_deinit() {
    list_del(projectiles);
    
    for (size_t i = 0; i < list_len(asteroids); i++) {
        asteroid_t* a = list_nth(asteroids, i);
        list_del(a->vertices);
    }
    
    list_del(asteroids);
}

void play_update(size_t w, size_t h, float frametime) {
    const Uint8* keys = SDL_GetKeyboardState(NULL);
    
    if (keys[SDL_GetScancodeFromKey(SDLK_ESCAPE)]) {
        state = STATE_MAIN_MENU;
        return;
    }
    
    if (list_len(asteroids) < floor(difficulty))
        create_asteroid(50);
    
    difficulty += frametime * 0.1;
    
    //Update Player
    if (keys[SDL_GetScancodeFromKey(left_key)])
        player.angle += frametime * 90;
    if (keys[SDL_GetScancodeFromKey(right_key)])
        player.angle -= frametime * 90;
    
    float player_dir_x = cos((player.angle+90)/180*3.14159);
    float player_dir_y = sin((player.angle+90)/180*3.14159);
    
    if (keys[SDL_GetScancodeFromKey(forward_key)]) {
        player.vel_x += player_dir_x * 25;
        player.vel_y += player_dir_y * 25;
    }
    
    player.fire_timeout -= frametime;
    if (keys[SDL_GetScancodeFromKey(fire_key)] && player.fire_timeout <= 0) {
        audio_play_sound(laser_sound[rand()%2], 1, 0, true);
        
        proj_t proj;
        proj.x = 0;
        proj.y = PLAYER_HEIGHT;
        rotate_point(&proj.x, &proj.y, player.angle);
        proj.x += player.x;
        proj.y += player.y;
        proj.vel_x = player_dir_x * 500;
        proj.vel_y = player_dir_y * 500;
        proj.dir_x = player_dir_x;
        proj.dir_y = player_dir_y;
        proj.angle = player.angle;
        list_append(projectiles, &proj);
        
        player.fire_timeout = 0.2;
    }
    
    player.x += player.vel_x * frametime;
    player.y += player.vel_y * frametime;
    player.vel_x *= frametime/(1/60.0) * 0.99;
    player.vel_y *= frametime/(1/60.0) * 0.99;
    float speed = sqrt(player.vel_x*player.vel_x + player.vel_y*player.vel_y);
    if (speed > 300) {
        player.vel_x = player.vel_x / speed * 300;
        player.vel_y = player.vel_y / speed * 300;
    }
    
    if (player.x < 0) player.x = WINDOW_WIDTH - fmod(fabs(player.x), WINDOW_WIDTH);
    if (player.y < 0) player.y = WINDOW_HEIGHT - fmod(fabs(player.y), WINDOW_HEIGHT);
    player.x = fmod(player.x, WINDOW_WIDTH);
    player.y = fmod(player.y, WINDOW_HEIGHT);
    
    //Update asteroids
    for (ptrdiff_t i = 0; i < list_len(asteroids); i++) {
        asteroid_t* a = list_nth(asteroids, i);
        
        if (list_len(a->vertices) <= 2) {
            list_remove(a);
            i--;
            continue;
        }
        
        a->angle += a->rotate_speed * frametime;
        a->cx += a->vel_x * frametime;
        a->cy += a->vel_y * frametime;
        if (a->cx < 0) a->cx = WINDOW_WIDTH - fmod(fabs(a->cx), WINDOW_WIDTH);
        if (a->cy < 0) a->cy = WINDOW_HEIGHT - fmod(fabs(a->cy), WINDOW_HEIGHT);
        a->cx = fmod(a->cx, WINDOW_WIDTH);
        a->cy = fmod(a->cy, WINDOW_WIDTH);
        
        ast_vert_t* v = list_nth(a->vertices, rand()%list_len(a->vertices));
        v->x += v->norm_x * frametime * 40;
        v->y += v->norm_y * frametime * 40;
        
        size_t req_count = fmin(asteroid_area(a) / 5000, 64);
        
        if (req_count <= 2) {
            list_remove(a);
            i--;
            continue;
        }
        
        while (list_len(a->vertices) < req_count) {
            size_t index = rand() % list_len(a->vertices);
            ast_vert_t* v0 = list_nth(a->vertices, index);
            ast_vert_t* v1 = list_nth(a->vertices, (index+1)%list_len(a->vertices));
            
            ast_vert_t res;
            res.x = (v0->x+v1->x) / 2;
            res.y = (v0->y+v1->y) / 2;
            res.norm_x = (v0->norm_x+v1->norm_x) / 2;
            res.norm_y = (v0->norm_y+v1->norm_y) / 2;
            float len = sqrt(res.norm_x*res.norm_x + res.norm_y*res.norm_y);
            res.norm_x /= len;
            res.norm_y /= len;
            res.x += res.norm_x * frametime * (rand()%256/128.0-1) * 10;
            res.y += res.norm_y * frametime * (rand()%256/128.0-1) * 10;
            
            list_insert(a->vertices, index+1, &res);
        }
        
        if (player_vs_asteroid(a)) {
            audio_play_sound(hurt_sound, 1, 0, true);
            
            player.health -= asteroid_area(a) / 1000000;
            break_asteroid(a, player_dir_x, player_dir_y);
            i--;
            continue;
        }
        
        for (ptrdiff_t j = 0; j < list_len(projectiles); j++) {
            proj_t* p = list_nth(projectiles, j);
            if (line_vs_asteroid(a, p->x, p->y, p->x+p->dir_x*PROJ_LEN,
                                    p->y+p->dir_y*PROJ_LEN)) {
                player.score += asteroid_area(a) / 100000;
                break_asteroid(a, p->dir_x, p->dir_y);
                list_remove(p);
                i--;
                j--;
                break;
            }
        }
    }
    
    //Update projectiles
    for (ptrdiff_t i = 0; i < list_len(projectiles); i++) {
        proj_t* p = list_nth(projectiles, i);
        p->x += p->vel_x * frametime;
        p->y += p->vel_y * frametime;
        if (p->x<0 || p->x>WINDOW_WIDTH || p->y<0 || p->y>WINDOW_HEIGHT) {
            list_remove(p);
            i--;
        }
    }
    
    if (player.health <= 0) state = STATE_LOST;
}

void play_draw(size_t w, size_t h) {
    //Draw projectiles
    for (size_t i = 0; i < list_len(projectiles); i++) {
        proj_t* p = list_nth(projectiles, i);
        draw_set_orientation(p->angle, p->x, p->y);
        draw_add_aabb(aabb_create_lbwh(p->x, p->y, 1, PROJ_LEN), draw_create_rgb(0.5, 1, 0.5));
    }
    draw_set_orientation(0, 0, 0);
    
    //Draw asteroids
    for (size_t i = 0; i < list_len(asteroids); i++) {
        asteroid_t* a = list_nth(asteroids, i);
        size_t c = list_len(a->vertices);
        draw_set_orientation(a->angle, a->cx, a->cy);
        for (size_t j = 0; j < c; j++) {
            ast_vert_t* v0 = list_nth(a->vertices, j);
            ast_vert_t* v1 = list_nth(a->vertices, (j+1)%c);
            float pos[] = {v0->x+a->cx, v0->y+a->cy, v1->x+a->cx,
                           v1->y+a->cy, a->cx, a->cy};
            draw_col_t col = draw_create_rgb(1, 0.5, 0.5);
            
            draw_col_t cols[] = {col, col, col};
            float uv[] = {0, 0, 0, 0, 0, 0};
            draw_add_tri(pos, cols, uv);
        }
    }
    draw_set_orientation(0, 0, 0);
    
    //Draw player
    draw_set_orientation(player.angle, player.x, player.y);
    float player_pos[] = {-PLAYER_WIDTH/2+player.x, player.y,
                          PLAYER_WIDTH/2+player.x, player.y,
                          player.x, player.y+PLAYER_HEIGHT};
    draw_col_t player_col = draw_create_rgb(0.05, 0.25, 1);
    draw_col_t player_cols[] = {player_col, player_col, player_col};
    float player_uv[] = {0, 0, 0, 0, 0, 0};
    draw_add_tri(player_pos, player_cols, player_uv);
    draw_set_orientation(0, 0, 0);
    
    draw_add_aabb(aabb_create_lbwh(0, 0, 20, 100), draw_create_rgba(1, 1, 1, 0.25));
    draw_add_aabb(aabb_create_lbwh(0, 0, 20, 100*player.health), draw_create_rgb(1, 1, 1));
    
    uint32_t text[256];
    utf32_format(text, 256, U"Score: %.0f", player.score);
    
    float text_w = font_drawn_width(font, text, 16);
    float pos[] = {WINDOW_WIDTH-text_w, 0};
    font_draw(font, text, pos, draw_create_rgb(1, 1, 1), 16);
    
    celika_set_title(U"Ludum Dare 35 - %.0f fps", 1/celika_get_display_frametime());
}
