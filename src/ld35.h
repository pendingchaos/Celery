#pragma once
#ifndef LD35_H
#define LD35_H

#include "celika/celika.h"

#define BUTTON_WIDTH 0.9
#define BUTTON_HEIGHT 50
#define BUTTON_TEXT_HEIGHT 40
#define BUTTON_PADDING 10
#define WINDOW_WIDTH 900
#define WINDOW_HEIGHT 900
#define PLAYER_WIDTH 40
#define PLAYER_HEIGHT 60
#define PROJ_LEN 20

typedef enum state_t {
    STATE_PLAYING,
    STATE_CONTROLS,
    STATE_MAIN_MENU,
    STATE_LOST
} state_t;

extern state_t state;
extern font_t* font;
extern sound_t* hurt_sound;
extern sound_t* laser_sound[2];
extern SDL_Keycode left_key;
extern SDL_Keycode right_key;
extern SDL_Keycode forward_key;
extern SDL_Keycode fire_key;

#endif
