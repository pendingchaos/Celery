#include "ld35.h"
#include "celika/celika.h"

#include <stdlib.h>
#include <time.h>

state_t state;
font_t* font;
sound_t* hurt_sound;
sound_t* laser_sound[2];
SDL_Keycode left_key;
SDL_Keycode right_key;
SDL_Keycode forward_key;
SDL_Keycode fire_key;

void play_init();
void play_deinit();
void play_update(size_t w, size_t h, float frametime);
void play_draw(size_t w, size_t h);

static bool button(float y, uint32_t* text) {
    aabb_t aabb = aabb_create_ce(WINDOW_WIDTH/2, y*(BUTTON_HEIGHT+BUTTON_PADDING)+(WINDOW_HEIGHT/2),
                                 WINDOW_WIDTH*BUTTON_WIDTH/2, BUTTON_HEIGHT/2);
    draw_add_aabb(aabb, draw_create_rgb(1, 1, 1));
    
    float text_width = font_drawn_width(font, text, BUTTON_TEXT_HEIGHT);
    
    float pos[] = {aabb.left+(aabb.width-text_width)/2,
                   aabb.bottom+(aabb.height-BUTTON_TEXT_HEIGHT)/2};
    font_draw(font, text, pos, draw_create_rgb(0, 0, 0), BUTTON_TEXT_HEIGHT);
    
    int mx, my;
    SDL_GetMouseState(&mx, &my);
    my = WINDOW_HEIGHT - my - 1;
    
    if (mx>aabb.left && mx<aabb_right(aabb) &&
        my>aabb.bottom && my<aabb_top(aabb)) {
        static int last_state = 0;
        bool res = !(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON_LMASK) &&
                    (last_state&SDL_BUTTON_LMASK);
        last_state = SDL_GetMouseState(NULL, NULL);
        return res;
    }
    
    return false;
}

static void key(int num, float y, uint32_t* label, SDL_Keycode* key, int* changing) {
    uint32_t text[256];
    uint32_t* key_utf32 = utf8_to_utf32((uint8_t*)SDL_GetKeyName(*key));
    if (*changing == num)
        utf32_format(text, sizeof(text), U"%s: Press key", label);
    else
        utf32_format(text, sizeof(text), U"%s: %s", label, key_utf32);
    free(key_utf32);
    
    if (button(y, text))
        *changing = num;
    
    if (*changing == num) {
        int key_count;
        const Uint8* keys = SDL_GetKeyboardState(&key_count);
        for (int i = 0; i < key_count; i++) {
            if (keys[i]) {
                *changing = -1;
                *key = SDL_GetKeyFromScancode(i);
                break;
            }
        }
    }
}

void celika_game_init(int* w, int* h) {
    srand(time(NULL));
    
    *w = WINDOW_WIDTH;
    *h = WINDOW_HEIGHT;
    
    font = font_create("Orbitron/Orbitron-Black.ttf");
    
    hurt_sound = audio_create_sound("hurt.ogg");
    laser_sound[0] = audio_create_sound("laser0.ogg");
    laser_sound[1] = audio_create_sound("laser1.ogg");
    
    state = STATE_MAIN_MENU;
    
    left_key = SDLK_a;
    right_key = SDLK_d;
    forward_key = SDLK_w;
    fire_key = SDLK_SPACE;
    
    play_init();
}

void celika_game_deinit() {
    play_deinit();
    
    audio_del_sound(laser_sound[1]);
    audio_del_sound(laser_sound[0]);
    audio_del_sound(hurt_sound);
    font_del(font);
}

void celika_game_event(SDL_Event event) {
}

void celika_game_frame(size_t w, size_t h, float frametime) {
    draw_add_aabb(aabb_create_lbwh(0, 0, w, h), draw_create_rgb(0, 0, 0));
    
    play_draw(w, h);
    
    switch (state) {
    case STATE_PLAYING:
        play_update(w, h, frametime);
        break;
    case STATE_MAIN_MENU:
        if (button(1, U"Play")) state = STATE_PLAYING;
        if (button(0, U"Controls")) state = STATE_CONTROLS;
        if (button(-1, U"Quit")) {
            SDL_Event event;
            event.type = SDL_QUIT;
            event.quit.timestamp = SDL_GetTicks();
            SDL_PushEvent(&event);
        }
        break;
    case STATE_CONTROLS: {
        static int changing_num = -1;
        key(0, 2, U"Turn Left", &left_key, &changing_num);
        key(1, 1, U"Turn Right", &right_key, &changing_num);
        key(2, 0, U"Move Forward", &forward_key, &changing_num);
        key(3, -1, U"Fire", &fire_key, &changing_num);
        if (button(-2, U"Back"))
            state = STATE_MAIN_MENU;
        break;
    }
    case STATE_LOST: {
        if (button(-0.5, U"Main Menu")) state = STATE_MAIN_MENU;
        if (button(0.5, U"Restart")) {
            play_deinit();
            play_init();
            state = STATE_PLAYING;
        }
        break;
    }
    }
}
